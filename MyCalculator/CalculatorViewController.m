//
//  CalculatorViewController.m
//  MyCalculator
//
//  Created by Karci Xie on 14-3-22.
//  Copyright (c) 2014年 Karci Xie. All rights reserved.
//

#import "CalculatorViewController.h"
#import <Foundation/NSString.h>
#import <Foundation/NSRange.h>

@interface CalculatorViewController ()

@end

double _currentValue;
double _firstValue;
double _secondValue;

UIButton *_currentOperator;
NSMutableArray *_currentInputArray;

@implementation CalculatorViewController

- (void) initCalculator
{
    _firstValue = 0;
    _secondValue = 0;
    _currentValue = 0;
    _currentOperator = nil;
    _currentInputArray = nil;
    self.currentValueLabel.text = [NSString stringWithFormat:@"0"];
}

- (void)viewDidLoad
{
    [self initCalculator];
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

+ (int) getRealNumber: (NSString*)inputChar
{
    if ([inputChar isEqualToString:@"0"]) {
        return 0;
    }
    if ([inputChar isEqualToString:@"1"]) {
        return 1;
    }
    if ([inputChar isEqualToString:@"2"]) {
        return 2;
    }
    if ([inputChar isEqualToString:@"3"]) {
        return 3;
    }
    if ([inputChar isEqualToString:@"4"]) {
        return 4;
    }
    if ([inputChar isEqualToString:@"5"]) {
        return 5;
    }
    if ([inputChar isEqualToString:@"6"]) {
        return 6;
    }
    if ([inputChar isEqualToString:@"7"]) {
        return 7;
    }
    if ([inputChar isEqualToString:@"8"]) {
        return 8;
    }
    if ([inputChar isEqualToString:@"9"]) {
        return 9;
    }
    return 0;
}

+ (double) returnRealValue: (NSMutableArray *)inputArray
{
    //convert _currentInputString to real value
    double tmpValue = 0.0;
    int switchNumber = 1;
    if (!inputArray)
    {
        return 0;
    }
    if ([[inputArray objectAtIndex:0]  isEqual: @"-"]) {
        [inputArray removeObjectAtIndex:0];
        switchNumber = -1;
    }
    
    NSMutableArray *integerArray = [NSMutableArray arrayWithCapacity:1];
    NSUInteger intArrayLength = 0;
    NSUInteger inputArrayCount = [inputArray count];
    //get the int part from inputArray
    for (intArrayLength = 0; intArrayLength < inputArrayCount; ++intArrayLength) {
        if ([[inputArray objectAtIndex:intArrayLength]  isEqual: @"."]) {
            break;
        } else
        {
            [integerArray addObject:[inputArray objectAtIndex:intArrayLength]];
        }
    }
    //the rest of inputArray will be decimal part
    if (inputArrayCount >= intArrayLength) {
        [inputArray removeObjectsInRange:NSMakeRange(0, intArrayLength)];
    }
    
    NSUInteger length = [inputArray count];
    NSLog(@"length of array is %d", length);
    if (length) {
        [inputArray removeObjectAtIndex:0];
        length = [inputArray count];
        for (int index = 0; index <= length - 1; index++) {
            NSLog(@"object at index %d is %@", index, (NSString*)[inputArray objectAtIndex:index]);
            tmpValue += [self getRealNumber:(NSString*)[inputArray objectAtIndex:index]]*pow(10.0, - 1 - index);
        }
    }
    
    for (int index = 0; index < intArrayLength; index++) {
        NSLog(@"object at index %d is %@", index, (NSString*)[integerArray objectAtIndex:index]);
        tmpValue += [self getRealNumber:(NSString*)[integerArray objectAtIndex:index]]*pow(10.0, intArrayLength - 1 - index);
    }
    
    NSLog(@" tmpValue is %f", tmpValue);
    return tmpValue*switchNumber;
}

- (NSMutableString *) toString: (NSMutableArray *)inputArray
{
    NSUInteger length = [inputArray count];
    NSMutableString * outputString = [NSMutableString stringWithCapacity:1];
    if (length < 1) {
        [outputString appendString:@"0"];
    }
    for (int i = 0; i < length; i++) {
        [outputString appendString:inputArray[i]];
    }
    return outputString;
}

- (IBAction) returnNumberN:(UIButton *)numberButton
{
    NSString *currentTitle = [numberButton currentTitle];
    if (![_currentInputArray count]) {
        _currentInputArray= [NSMutableArray arrayWithCapacity:10];
        [_currentInputArray addObject:currentTitle];
    } else if ([_currentInputArray count] < 1) {
        [_currentInputArray addObject:currentTitle];
    } else if ([[_currentInputArray objectAtIndex:0] isEqual:@"0"]) {
        [_currentInputArray replaceObjectAtIndex:0 withObject:currentTitle];
    } else if ([[_currentInputArray objectAtIndex:0] isEqual:@"."]) {
        [_currentInputArray replaceObjectAtIndex:0 withObject:@"0"];
        [_currentInputArray addObject:currentTitle];
    } else {
        [_currentInputArray addObject:currentTitle];
    }
    self.currentValueLabel.text = [self toString:_currentInputArray];
}

- (IBAction) clickOperatorButton: (UIButton*)operatorButton
{
    _currentOperator = operatorButton;
    NSString *currentOperator = [operatorButton currentTitle];
    NSLog(@"click add button %@", currentOperator);
    NSLog(@"===_currentInputArray===%@", _currentInputArray);
    _firstValue = [CalculatorViewController returnRealValue:_currentInputArray];
    NSLog(@" first value is %f", _firstValue);
    self.currentValueLabel.text = [NSString stringWithFormat:@"%f", _firstValue];
    _currentInputArray = nil;
}

- (IBAction)clickOperatorEqual
{
    NSString *currentOperator = [_currentOperator currentTitle];
    _secondValue = [CalculatorViewController returnRealValue:_currentInputArray];
    if ([currentOperator isEqual:@"+"]) {
        _currentValue = _firstValue + _secondValue;
    }
    if ([currentOperator isEqual:@"-"]) {
        _currentValue = _firstValue - _secondValue;
    }
    if ([currentOperator isEqual:@"*"]) {
        _currentValue = _firstValue * _secondValue;
    }
    if ([currentOperator isEqual:@"/"]) {
        if (_secondValue == 0) {
            _currentValue = 0;
        }
        else
        {
            _currentValue = _firstValue / _secondValue;
        }
    }
    self.currentValueLabel.text = [NSString stringWithFormat:@"%f", _currentValue];
    _currentInputArray = nil;
}

- (IBAction) clickClearButton
{
   [self initCalculator];
}

- (IBAction) clickBackButton
{
    NSUInteger length = [_currentInputArray count];
    if (length >= 1) {
        [_currentInputArray removeObjectAtIndex:(length - 1)];
    }
    self.currentValueLabel.text = [self toString:_currentInputArray];
}

- (IBAction) clickSwitchButton
{
    NSUInteger length = [_currentInputArray count];
    if (length < 1) {
        _currentInputArray= [NSMutableArray arrayWithCapacity:10];
        [_currentInputArray addObject:@"-"];
    } else if([[_currentInputArray objectAtIndex:0]  isEqual: @"-"])
    {
        [_currentInputArray removeObjectAtIndex:0];
    } else
    {
        [_currentInputArray addObject:@"-"];
        for (int i = 0; i < length; ++i) {
            [_currentInputArray exchangeObjectAtIndex:(length - i) withObjectAtIndex:(length - i - 1)];
        }
    }
    self.currentValueLabel.text = [self toString:_currentInputArray];
}

@end
