//
//  CalculatorViewController.h
//  MyCalculator
//
//  Created by Karci Xie on 14-3-22.
//  Copyright (c) 2014年 Karci Xie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CalculatorViewController : UIViewController


@property (nonatomic, weak) IBOutlet UILabel *currentValueLabel;

- (IBAction) returnNumberN:(UIButton *)button;
- (IBAction) clickOperatorEqual;
- (IBAction) clickOperatorButton: (UIButton *)operatorButton;
- (IBAction) clickClearButton;
- (IBAction) clickBackButton;
- (IBAction) clickSwitchButton;

- (void) initCalculator;
- (NSMutableString *) toString: (NSMutableArray *)inputArray;

@end
