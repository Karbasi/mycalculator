//
//  main.m
//  MyCalculator
//
//  Created by Karci Xie on 14-3-22.
//  Copyright (c) 2014年 Karci Xie. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CalculatorAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CalculatorAppDelegate class]));
    }
}
