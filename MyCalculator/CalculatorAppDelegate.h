//
//  CalculatorAppDelegate.h
//  MyCalculator
//
//  Created by Karci Xie on 14-3-22.
//  Copyright (c) 2014年 Karci Xie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CalculatorAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
